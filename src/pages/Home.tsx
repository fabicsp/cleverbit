import { FC } from "react";
import Post from "../components/Post";

import { getPosts } from "../mock/post";

const Home: FC = () => {
  const posts = getPosts();

  return (
    <section className="posts-container">
      {posts.map((post) => {
        return <Post key={post.id} post={post} />;
      })}
    </section>
  );
};

export default Home;
