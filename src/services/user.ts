import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const userApi = createApi({
  reducerPath: "user",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:8080/api/user",
    headers: { token: "demo" },
  }),
  endpoints: (builder) => ({
    getUser: builder.query({
      query: (userId: number) => {
        console.log("USERID: ", userId);
        return "";
      },
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved, ...rest }
      ) {
        const ws = new WebSocket("ws://localhost:8080/user/5"); // TODO: change 5 to userId
        try {
          // wait for the initial query to resolve before proceeding
          await cacheDataLoaded;

          // when data is received from the socket connection to the server,
          // if it is a message and for the appropriate channel,
          // update our query result with the received message
          const listener = (event: unknown) => {
            updateCachedData((prev) => {
              return { ...prev, test: 1234 };
            });
          };

          console.log("ws: ", ws);
          ws.addEventListener("message", listener);
        } catch {
          // no-op in case `cacheEntryRemoved` resolves before `cacheDataLoaded`,
          // in which case `cacheDataLoaded` will throw
          console.log("WS run in error");
        }
        // cacheEntryRemoved will resolve when the cache subscription is no longer active
        await cacheEntryRemoved;
        // perform cleanup steps once the `cacheEntryRemoved` promise resolves
        ws.close();
      },
    }),
  }),
});
