import "./App.css";

import { FC } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";

import Home from "./pages/Home";
import Comments from "./pages/Comments";
import Layout from "./components/Layout";

const App: FC = () => {
  return (
    <Router>
      <Routes>
        <Route
          path="/comments"
          caseSensitive={false}
          element={
            <Layout>
              <Comments />
            </Layout>
          }
        />
        <Route
          path="/"
          caseSensitive={false}
          element={
            <Layout>
              <Home />
            </Layout>
          }
        />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </Router>
  );
};

export default App;
