import "./Post.css";

import React, { FC, useState } from "react";
import dayjs from "dayjs";

import { ReactComponent as LikeSvg } from "../../svg/Like.svg";
import { post } from "../../mock/post";
import { getComments } from "../../mock/comment";

const Post: FC<{
  post: typeof post;
}> = ({ post }) => {
  const like = () => {
    console.log("Like");
  };

  const defaultComments = getComments(2);

  const [comment, setComment] = useState("");

  const submit = (event: React.BaseSyntheticEvent) => {
    event.preventDefault();
    setComment(""); // TODO: send it to the backend before reset
  };

  return (
    <article className="post-container">
      <header>
        <h2>{post.title}</h2>
        <div className="post-likes">
          {post.likes}
          <LikeSvg onClick={like} className="like-post" />
        </div>
      </header>
      <p>{post.description}</p>
      <form onSubmit={submit}>
        <input
          placeholder="Add your comment here..."
          value={comment}
          onChange={(event: React.BaseSyntheticEvent) =>
            setComment(event.target.value)
          }
        />
      </form>

      {defaultComments.map((defaultComment) => {
        return (
          <div key={defaultComment.id}>
            <p>{defaultComment.content}</p>
            <span className="comment-date">
              {dayjs(defaultComment.createdAt).format("YYYY/MM/DD")}
            </span>
          </div>
        );
      })}
    </article>
  );
};

export default Post;
