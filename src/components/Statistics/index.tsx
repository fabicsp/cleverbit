import "./Statistics.css";

import { FC, useState } from "react";

import { userApi } from "../../services/user";

const Statistics: FC = () => {
  const [comments] = useState(2);

  const data = userApi.useGetUserQuery(2); // TODO: Deal with the userId

  console.log("DATA: ", data);

  const reload = () => console.log("Reloading Posts...");

  return (
    <section className="statistic-container">
      <button onClick={reload}>Reload</button>
      <div className="interactive-container">
        <span>Likes: {"data"}</span>
        <span>Comments: {comments}</span>
      </div>
    </section>
  );
};

export default Statistics;
