import "./Layout.css";

import { FC, ReactNode } from "react";
import { Link } from "react-router-dom";

import Statistics from "../Statistics";

const Layout: FC<{
  children: ReactNode;
}> = ({ children }) => {
  return (
    <>
      <div id="layout">
        <nav>
          <Link to="/">Posts</Link>
          <Link to="/comments">Comments</Link>
        </nav>
      </div>
      <Statistics />
      {children}
    </>
  );
};

export default Layout;
