export const user = {
  id: 1,
  statistics: {
    likes: 2,
    comments: 3,
  },
  createdAt: new Date(),
};
