export const comment = {
  id: 1,
  content:
    "It is a long estabilished fact that a reader will be distracted by the readable content of the page when looking at its layout.",

  userId: 2,
  createdAt: new Date(),
};

const generateMockComment = (): typeof comment => ({
  id: Math.random(),
  content: comment.content,
  userId: Math.floor(Math.random() * 10) + 1,
  createdAt: new Date(),
});

export const getComments = (amount = 5) =>
  Array.from(Array(amount).keys()).map(() => generateMockComment());
